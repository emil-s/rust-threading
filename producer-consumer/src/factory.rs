use crate::product_list::ProductList;
use owo_colors::*;
use rand::{thread_rng, Rng};
use std::sync::{Arc, Mutex};
use std::thread::{sleep, spawn};
use std::time::Duration;

pub struct Factory {
    products: Arc<Mutex<ProductList>>,
}

impl Factory {
    pub fn new() -> Self {
        Self {
            products: Arc::new(Mutex::new(ProductList::new())),
        }
    }

    pub fn run(&mut self) {
        let producer = {
            let products = self.products.clone();
            let prefix = "[PRODUCER]".green();
            spawn(move || loop {
                sleep(Duration::from_millis(thread_rng().gen_range(10..300)));
                println!("{prefix}: {}", Factory::produce(&products))
            })
        };

        let consumer = {
            let products = self.products.clone();
            let prefix = "[CONSUMER]".red();
            spawn(move || loop {
                sleep(Duration::from_millis(thread_rng().gen_range(10..300)));
                println!("{prefix}: {}", Factory::consume(&products))
            })
        };

        producer.join().unwrap();
        consumer.join().unwrap();
    }

    fn produce(products: &Arc<Mutex<ProductList>>) -> String {
        match products.lock().unwrap().add() {
            Some(index) => format!("Produced product on {index}"),
            None => format!("Failed to produce product!"),
        }
    }

    fn consume(products: &Arc<Mutex<ProductList>>) -> String {
        match products.lock().unwrap().remove() {
            Some(index) => format!("Consumed product on {index}"),
            None => format!("Failed to consume product!"),
        }
    }
}
