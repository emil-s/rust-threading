// This file holds on to my ProductList, which works using an underlying fixed size array that handles the data.
// I did this abstraction in order to interact with the array as if it was a queue, simplifying the Factory.

#[derive(Clone, Copy)]
pub struct ProductList {
    buffer: [bool; 3],
    index: u8,
}

impl ProductList {
    pub fn new() -> Self {
        Self {
            buffer: [false; 3],
            index: 0,
        }
    }

    pub fn add(&mut self) -> Option<u8> {
        if self.index == 3 {
            return None;
        }

        self.buffer[self.index as usize] = true;
        self.index += 1;

        Some(self.index - 1)
    }

    pub fn remove(&mut self) -> Option<u8> {
        if self.index == 0 {
            return None;
        }

        self.index -= 1;
        self.buffer[self.index as usize] = false;

        Some(self.index)
    }
}
