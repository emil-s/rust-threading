mod factory;
mod product_list;

use factory::Factory;

fn main() {
    #[cfg(windows)]
    {
        colored::control::set_virtual_terminal(true).unwrap();
    }

    let mut factory = Factory::new();

    factory.run();
}
