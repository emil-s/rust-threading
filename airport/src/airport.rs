use crate::buffer::Buffer;
use crate::check_in_station::CheckInStation;
use crate::gate::Gate;
use std::sync::{Arc, Mutex};

pub struct Airport {
    buffer: Arc<Mutex<Buffer>>,
    check_in_stations: Vec<CheckInStation>,
    gates: Vec<Gate>,
}

const NUM_CHECK_IN_STATIONS: usize = 3;
const NUM_GATES: usize = 3;

impl Airport {
    pub fn new() -> Self {
        let buffer = Arc::new(Mutex::new(Buffer::new()));

        let check_in_stations = (0..NUM_CHECK_IN_STATIONS)
            .into_iter()
            .map(|i| CheckInStation::new(buffer.clone(), i + 1))
            .collect();

        let gates = (0..NUM_GATES)
            .into_iter()
            .map(|i| Gate::new(buffer.clone(), i + 1))
            .collect();

        Self {
            buffer,
            check_in_stations,
            gates,
        }
    }
}
