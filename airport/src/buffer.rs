use crate::luggage::Luggage;

#[derive(Debug, Clone, Copy)]
pub struct Buffer {
    buffer: [Option<Luggage>; BUFFER_SIZE],
    amount: usize,
}

const BUFFER_SIZE: usize = 300;

impl Buffer {
    pub fn new() -> Self {
        Self {
            buffer: [None; BUFFER_SIZE],
            amount: 0,
        }
    }

    // If there's space for it, it adds the item to the buffer, and returns None.
    // Otherwise it returns the item back to caller.
    pub fn add(&mut self, item: Luggage) -> Option<Luggage> {
        if self.amount == BUFFER_SIZE {
            return Some(item);
        }

        self.buffer[self.amount as usize] = Some(item);

        self.amount += 1;

        None
    }

    // If there are any items in the buffer, it'll return the last item.
    // Otherwise it'll return None.
    pub fn pop(&mut self) -> Option<Luggage> {
        if self.amount == 0 {
            return None;
        }

        self.amount -= 1;

        let output = self.buffer[self.amount as usize];
        self.buffer[self.amount as usize] = None;

        output
    }
}
