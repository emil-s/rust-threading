use crate::luggage::LuggageType;
use owo_colors::OwoColorize;
use rand::Rng;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::thread::{self, JoinHandle};
use std::time::Duration;

use crate::buffer::Buffer;
use crate::luggage::Luggage;

pub struct CheckInStation {
    is_open: bool,
    station_id: usize,
    name: String,
    handle: JoinHandle<()>,
}

impl CheckInStation {
    pub fn new(buffer: Arc<Mutex<Buffer>>, station_id: usize) -> Self {
        let is_open = true;
        let name = format!("Check-In Station #{}", station_id);
        let name_clone = name.clone();
        let handle = thread::spawn(move || {
            let mut rng = rand::thread_rng();
            loop {
                let sleep_duration = rng.gen_range(5..=15);
                thread::sleep(Duration::from_secs(sleep_duration));

                let should_open: bool = rng.gen();
                if should_open {
                    let luggage_count = rng.gen_range(50..=80);
                    let luggage_type_count = Self::generate_luggage(luggage_count, &buffer);

                    Self::print_luggage_type_count(&name_clone, &luggage_type_count);
                } else {
                    println!("[{}]: is closed", name_clone.green());
                }
            }
        });

        Self {
            is_open,
            station_id,
            name,
            handle,
        }
    }

    fn print_luggage_type_count(name: &str, luggage_type_count: &HashMap<LuggageType, u32>) {
        println!(
            "[{}]: added luggage: ({}).",
            name.green(),
            luggage_type_count
                .iter()
                .map(|(luggage_type, count)| format!("{luggage_type}: {count}"))
                .collect::<Vec<_>>()
                .join(", ")
        );
    }

    // Makes the printing look pretty. :)
    fn generate_luggage(
        luggage_count: usize,
        buffer: &Arc<Mutex<Buffer>>,
    ) -> HashMap<LuggageType, u32> {
        let mut buffer = buffer.lock().unwrap();
        (0..luggage_count)
            .map(|_| Luggage::new())
            .filter(|luggage| buffer.add(luggage.clone()).is_none())
            .fold(HashMap::new(), |mut acc, luggage| {
                *acc.entry(luggage.get_luggage_type()).or_insert(0) += 1;
                acc
            })
    }
}
