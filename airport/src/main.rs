mod airport;
mod buffer;
mod check_in_station;
mod gate;
mod luggage;

use airport::Airport;

fn main() {
    #[cfg(windows)]
    {
        colored::control::set_virtual_terminal(true).unwrap();
    }

    let _airport = Airport::new();

    std::io::stdin().read_line(&mut String::new()).unwrap();
}
