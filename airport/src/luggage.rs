use owo_colors::OwoColorize;
use rand::{
    distributions::{Distribution, Standard},
    Rng,
};
use std::fmt;
use std::sync::atomic::{AtomicU64, Ordering};

#[derive(Debug, Clone, Copy)]
pub struct Luggage {
    luggage_type: LuggageType,
    id: u64,
}

static LUGGAGE_ID_COUNTER: AtomicU64 = AtomicU64::new(1);

impl Luggage {
    pub fn new() -> Self {
        let id = LUGGAGE_ID_COUNTER.fetch_add(1, Ordering::SeqCst);
        Self {
            luggage_type: rand::random(),
            id,
        }
    }

    pub fn get_id(&self) -> u64 {
        self.id
    }

    pub fn get_luggage_type(&self) -> LuggageType {
        self.luggage_type
    }
}

impl fmt::Display for LuggageType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let output = match self {
            LuggageType::Suitcase => format!("{}", "suitcase".blue()),
            LuggageType::Animal => format!("{}", "animal".red()),
            LuggageType::Bag => format!("{}", "bag".green()),
            LuggageType::DuffelBag => format!("{}", "duffel bag".magenta()),
            LuggageType::GarmentBag => format!("{}", "garment bag".yellow()),
        };
        write!(f, "{output}")
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum LuggageType {
    Suitcase,
    Animal,
    Bag,
    DuffelBag,
    GarmentBag,
}

impl Distribution<LuggageType> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> LuggageType {
        match rng.gen_range(0..5) {
            0 => LuggageType::Suitcase,
            1 => LuggageType::Animal,
            2 => LuggageType::Bag,
            3 => LuggageType::DuffelBag,
            _ => LuggageType::GarmentBag,
        }
    }
}
