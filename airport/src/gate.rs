use owo_colors::OwoColorize;
use rand::Rng;
use std::sync::{Arc, Mutex};
use std::thread::{self, JoinHandle};
use std::time::Duration;

use crate::buffer::Buffer;

pub struct Gate {
    is_open: bool,
    gate_id: usize,
    name: String,
    handle: JoinHandle<()>,
}

impl Gate {
    pub fn new(buffer: Arc<Mutex<Buffer>>, gate_id: usize) -> Self {
        let is_open = true;
        let name = format!("Gate #{}", gate_id + 1);
        let name_clone = name.clone();
        let handle = thread::spawn(move || {
            let mut rng = rand::thread_rng();
            loop {
                let sleep_duration = rng.gen_range(1..=5);
                thread::sleep(Duration::from_secs(sleep_duration));

                // Randomly decide whether the gate should be open or closed.
                match rng.gen::<bool>() {
                    true => Self::perform_gate_action(&buffer, &name_clone),
                    false => println!("[{}]: is cloed.", name_clone.red()),
                }
            }
        });

        Self {
            is_open,
            gate_id,
            name,
            handle,
        }
    }

    fn perform_gate_action(buffer: &Arc<Mutex<Buffer>>, name: &str) {
        match buffer.lock().unwrap().pop() {
            Some(luggage) => println!(
                "[{}] took out {} with id: {}",
                name.red(),
                luggage.get_luggage_type(),
                luggage.get_id()
            ),
            None => println!("[{}]: Unable to get luggage into airport.", name.red()),
        }
    }
}
