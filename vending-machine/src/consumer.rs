use crate::bottle::Bottle;
use crate::buffer::Buffer;
use owo_colors::OwoColorize;
use rand::{thread_rng, Rng};
use std::sync::{Arc, Mutex};
use std::thread::{sleep, spawn};
use std::time::Duration;

pub fn run_consumer(name: String, buffer: Arc<Mutex<Buffer>>) {
    spawn(move || {
        let name = name.red();

        loop {
            sleep(Duration::from_millis(thread_rng().gen_range(50..2000)));
            let buffer_output = buffer.lock().unwrap().pop();

            if let Err(_) = buffer_output {
                println!("[{name}]: Unable to get bottle.");
                continue;
            }

            let (bottle, index) = buffer_output.unwrap();

            if let Bottle::Branded(bottle) = bottle {
                println!("[{name}]: Consumed {bottle} at {index}")
            }
        }
    });
}
