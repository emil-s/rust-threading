mod bottle;
mod bottle_factory;
mod buffer;
mod consumer;
mod sorting_facility;

use bottle_factory::BottleFactory;
use consumer::run_consumer;
use sorting_facility::SortingFacility;

fn main() {
    #[cfg(windows)]
    {
        // Fixes some wacky ANSI color handling on Windows Command Prompt and Powershell.
        colored::control::set_virtual_terminal(true).unwrap();
    }

    let mut factory = BottleFactory::new();

    factory.run_producer();

    let sorter = SortingFacility::new();

    let buffers = sorter.get_buffers();

    let consumer_names = ["BEER CONSUMER", "SODA CONSUMER", "COFFEE CONSUMER"];

    buffers
        .into_iter()
        .zip(consumer_names.iter())
        .for_each(|(buffer, name)| run_consumer(name.to_string(), buffer));

    sorter.run_sorter(factory.get_buffer());
    std::io::stdin().read_line(&mut String::new()).unwrap();
}
