use owo_colors::OwoColorize;
use rand::{thread_rng, Rng};
use std::sync::{Arc, Mutex};
use std::thread::{sleep, spawn, JoinHandle};
use std::time::Duration;

use crate::bottle::{Bottle, BrandedBottle, GenericBottle};
use crate::buffer::Buffer;

pub struct SortingFacility {
    beer_buffer: Arc<Mutex<Buffer>>,
    soda_buffer: Arc<Mutex<Buffer>>,
    coffee_buffer: Arc<Mutex<Buffer>>,
}

impl SortingFacility {
    pub fn new() -> Self {
        Self {
            beer_buffer: Arc::new(Mutex::new(Buffer::new())),
            soda_buffer: Arc::new(Mutex::new(Buffer::new())),
            coffee_buffer: Arc::new(Mutex::new(Buffer::new())),
        }
    }

    pub fn run_sorter(mut self, input_buffer: Arc<Mutex<Buffer>>) -> JoinHandle<()> {
        let name = "BOTTLE SORTER".blue();

        spawn(move || loop {
            sleep(Duration::from_millis(thread_rng().gen_range(50..2000)));
            let buffer_output = input_buffer.lock().unwrap().pop();

            if let Err(_) = buffer_output {
                println!("[{name}]: Unable to get bottle from factory.");
                continue;
            }

            let (bottle, _) = buffer_output.unwrap();

            if let Bottle::Generic(bottle) = bottle {
                match self.insert_bottle(bottle) {
                    (Ok(index), bottle_type) => {
                        println!("[{name}]: Inserted {bottle_type} into {index}.")
                    }
                    (Err(_), bottle_type) => {
                        println!("[{name}]: Failed to insert {bottle_type}.")
                    }
                }
            }
        })
    }

    fn insert_bottle(&mut self, _bottle: GenericBottle) -> (Result<usize, Bottle>, BrandedBottle) {
        match thread_rng().gen_range(0..3) {
            0 => (
                self.beer_buffer
                    .lock()
                    .unwrap()
                    .add(Bottle::Branded(BrandedBottle::Beer)),
                BrandedBottle::Beer,
            ),
            1 => (
                self.soda_buffer
                    .lock()
                    .unwrap()
                    .add(Bottle::Branded(BrandedBottle::Soda)),
                BrandedBottle::Soda,
            ),
            _ => (
                self.coffee_buffer
                    .lock()
                    .unwrap()
                    .add(Bottle::Branded(BrandedBottle::Coffee)),
                BrandedBottle::Coffee,
            ),
        }
    }

    pub fn get_buffers(&self) -> Vec<Arc<Mutex<Buffer>>> {
        vec![
            self.beer_buffer.clone(),
            self.soda_buffer.clone(),
            self.coffee_buffer.clone(),
        ]
    }
}
