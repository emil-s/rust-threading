use crate::bottle::Bottle;

#[derive(Debug, Clone, Copy)]
pub struct Buffer {
    buffer: [Option<Bottle>; BUFFER_SIZE],
    amount: usize,
}

const BUFFER_SIZE: usize = 10;

impl Buffer {
    pub fn new() -> Self {
        Self {
            buffer: [None; BUFFER_SIZE],
            amount: 0,
        }
    }

    // If there is space for it, give the index that the bottle was put into.
    // Otherwise, return back the bottle.
    pub fn add(&mut self, item: Bottle) -> Result<usize, Bottle> {
        if self.amount == BUFFER_SIZE {
            return Err(item);
        }

        self.buffer[self.amount as usize] = Some(item);

        self.amount += 1;

        Ok(self.amount)
    }

    // If there are any items in the buffer, give the Bottle and the index of the last item.
    // Otherwise, don't return anything.
    pub fn pop(&mut self) -> Result<(Bottle, usize), ()> {
        if self.amount == 0 {
            return Err(());
        }

        self.amount -= 1;

        let output = self.buffer[self.amount as usize];
        self.buffer[self.amount as usize] = None;

        Ok((output.unwrap(), self.amount))
    }
}
