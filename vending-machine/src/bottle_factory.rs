use owo_colors::OwoColorize;
use rand::{thread_rng, Rng};

use crate::bottle::{Bottle, GenericBottle};
use crate::buffer::Buffer;
use std::sync::{Arc, Mutex};
use std::thread::{sleep, spawn};
use std::time::Duration;

pub struct BottleFactory {
    buffer: Arc<Mutex<Buffer>>,
}

impl BottleFactory {
    pub fn new() -> Self {
        Self {
            buffer: Arc::new(Mutex::new(Buffer::new())),
        }
    }

    pub fn get_buffer(&mut self) -> Arc<Mutex<Buffer>> {
        self.buffer.clone()
    }

    pub fn run_producer(&mut self) {
        let buffer = self.buffer.clone();
        let name = "BOTTLE PRODUCER".green();

        spawn(move || loop {
            sleep(Duration::from_millis(thread_rng().gen_range(50..2000)));
            match buffer.lock().unwrap().add(Bottle::Generic(GenericBottle)) {
                Ok(index) => println!("[{name}]: Inserted bottle into buffer at {index}."),
                Err(_) => println!("[{name}]: Failed to insert bottle into buffer."),
            }
        });
    }
}
