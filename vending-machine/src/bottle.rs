use std::fmt;

#[derive(Debug, Clone, Copy)]
pub enum Bottle {
    Branded(BrandedBottle),
    Generic(GenericBottle),
}
#[derive(Debug, Clone, Copy)]
pub struct GenericBottle;

#[derive(Debug, Clone, Copy)]
pub enum BrandedBottle {
    Beer,
    Soda,
    Coffee,
}

impl fmt::Display for BrandedBottle {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let output = match self {
            Self::Beer => "beer",
            Self::Soda => "soda",
            Self::Coffee => "coffee",
        };
        write!(f, "{output}")
    }
}
